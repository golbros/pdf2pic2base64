const fs = require('fs');
const cors = require('cors');
const express = require("express");
const multer = require("multer");
const upload = multer({ dest: "pdf-uploads/" }); // <= Directory for the file-uploads
const { fromPath } = require("pdf2pic");
require('events').EventEmitter.defaultMaxListeners = 3;

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/* The field-name 'pdf-file' has to be used as key which the pdf-file
   to upload lies on in the FormData-Object at the frontend. */
app.post("/upload_files", upload.array("pdf-file"), uploadFiles);

app.listen(8089, () => {
  console.log(`Server started...`);
});

async function uploadFiles(req, res) {

  const options = { // Settings for the pdf-images
    width: 2550,
    height: 3300,
    density: 330,
    format: 'png',
  };

  let allPDFImages = [];
  for (const file of req.files) {
    const convert = fromPath(file.path, options);
    const pages = await convert.bulk(-1, true);
    allPDFImages = [...allPDFImages, ...pages]
    fs.unlinkSync(file.path);
  }
  res.json(allPDFImages);
}
